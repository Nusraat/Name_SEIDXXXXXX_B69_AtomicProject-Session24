<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\BookTitle\BookTitle;


$obj = new BookTitle();
$obj->setData($_GET);

$oneData  =  $obj->view();


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>



    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>




</head>
<body>

<div class="container">


<?php

         echo "
             <h1> Single Book Information </h1>
               
             <table class='table table-bordered table-striped'>
             
                    <tr>                   
                        <td>  <b>ID</b>  </td>                
                        <td>  <b>$oneData->id</b>  </td>                
                      
                    </tr>
        
                     <tr>                   
                        <td>  <b>Book Title</b>  </td>                
                        <td>  <b>$oneData->book_title</b>  </td>                
                      
                    </tr>
                         
                     <tr>                   
                        <td>  <b>Author Name</b>  </td>                
                        <td>  <b>$oneData->author_name</b>  </td>                
                      
                    </tr>
                
             
             </table>

         ";


?>

</div>

</body>
</html>